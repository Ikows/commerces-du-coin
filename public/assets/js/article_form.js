$('.article-form-images-toggler').click(function () {
  $(this).next().slideToggle()
})

// File input & cropper
$(document).ready(function () {
  bsCustomFileInput.init()
})

// Cropper Main.
const mainImage = document.getElementById('article-form-mainImage-preview');
const cropper0 = new Cropper(mainImage, {
  aspectRatio: 4 / 3,
  crop(event) {
    $('#article_mainImageX').val(event.detail.x)
    $('#article_mainImageY').val(event.detail.y)
    $('#article_mainImageW').val(event.detail.width)
    $('#article_mainImageH').val(event.detail.height)
  },
});

const reader0 = new FileReader();
reader0.onload = function (e) {
  $('#article-form-mainImage-preview').attr('src', e.target.result);
  cropper0.replace(e.target.result)
  $('#mainCropperWrapper').show()
}

$('#article_mainImage').change(function () {
  if (this.files && this.files[0]) {
    reader0.readAsDataURL(this.files[0]);
  }
});

// Cropper Secs.
var croppers = [];
var readers = [];
for (let i = 1; i < 5; i++) {
  var secImage1 = document.getElementById('article-form-image-preview-' + i);
  croppers[i] = new Cropper(secImage1, {
    aspectRatio: 4 / 3,
    crop(event) {
      $('#article_XsecImage' + i).val(event.detail.x)
      $('#article_YsecImage' + i).val(event.detail.y)
      $('#article_WsecImage' + i).val(event.detail.width)
      $('#article_HsecImage' + i).val(event.detail.height)
    },
  });

  readers[i] = new FileReader();
  readers[i].onload = function (e) {
    $('#article-form-image-preview-' + i).attr('src', e.target.result);
    croppers[i].replace(e.target.result)
    $('#mainCropperWrapper-' + i).show()
  }

  $('#article_secImage' + i).change(function () {
    if (this.files && this.files[0]) {
      readers[i].readAsDataURL(this.files[0]);
    }
  });
}

// Edit change image
$('.article-form-image-change').click(function () {
  $(this).parent().parent().prev().prev().show()
  $(this).parent().parent().remove()
})