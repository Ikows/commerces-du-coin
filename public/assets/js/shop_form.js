// Cropper.
const image = document.getElementById('shop-form-image-preview');
const cropper = new Cropper(image, {
  aspectRatio: 16 / 9,
  crop(event) {
    $('#shop_imageX').val(event.detail.x)
    $('#shop_imageY').val(event.detail.y)
    $('#shop_imageW').val(event.detail.width)
    $('#shop_imageH').val(event.detail.height)
  },
});

// File input & cropper
$(document).ready(function () {
  bsCustomFileInput.init()
})

var reader = new FileReader();
reader.onload = function (e) {
  $('#shop-form-image-preview').attr('src', e.target.result);
  cropper.replace(e.target.result)
  $('.cropperWrapper').show()
}

function readURL(input) {
  if (input.files && input.files[0]) {
    reader.readAsDataURL(input.files[0]);
  }
}

$('input[type=file]').change(function () {
  readURL(this);
});

// Edit change image TODO A traiter en ajax
// $('#shop-form-image-change').click(function () {
//   $('.existingContainer').remove()
//   $('#shop_mainImage').parent().parent().show()
// })



//Autocomplete
let isAutoListCityVisible = false;
$('#shop_city').on('keyup', function () {
  const query = $(this).val();
  if (query.length > 0) {
    $.ajax({
        url: `/geo/api/auto?q=${query}&type=municipality`
      })
      .done(function (data) {
        if (data.features.length > 0) {
          let autoList = $('<ul class="autoList municipalityAutoList"></ul>')
          data.features.forEach(element => {
            let li = `<li 
            class="autoItem cityItem" 
            data-attr-postcode="${element.properties.postcode}" 
            data-attr-citycode="${element.properties.citycode}" 
            data-attr-val="${element.properties.label}"><p><strong>${element.properties.label}</strong></p><p>${element.properties.context}</p></li>`
            autoList.append(li)
          });
          if (isAutoListCityVisible === false) {
            $('#shop_city').parent().append(autoList)
            isAutoListCityVisible = true;
          } else {
            $('.autoList.municipalityAutoList').replaceWith(autoList)
          }
        } else {
          $('.autoList.municipalityAutoList').remove()
          isAutoListCityVisible = false;

        }
      });
  } else {
    $('.autoList.municipalityAutoList').remove()
    isAutoListCityVisible = false;
  }
})

$(document).on('click', '.cityItem', function () {
  $('#shop_city').val($(this).attr('data-attr-val'))
  $('#shop_citycode').val($(this).attr('data-attr-citycode'))
  $('#shop_zipCode').val($(this).attr('data-attr-postcode'))
  isAutoListCityVisible = false;
})

// Adresse.
let isAutoListAddressVisible = false;
$('#shop_address').on('keyup', function () {
  const query = $(this).val();
  if (query.length > 0) {
    $.ajax({
        url: `/geo/api/auto?q=${query}&type=housenumber&citycode=${$('#shop_citycode').val()}`
      })
      .done(function (data) {
        if (data.features.length > 0) {
          let autoList = $('<ul class="autoList housenumberAutoList"></ul>')
          data.features.forEach(element => {
            let li = `<li 
            class="autoItem addressItem" 
            data-attr-val="${element.properties.name}"><p><strong>${element.properties.label}</strong></p><p>${element.properties.context}</p></li>`
            autoList.append(li)
          });
          if (isAutoListAddressVisible === false) {
            $('#shop_address').parent().append(autoList)
            isAutoListAddressVisible = true;
          } else {
            $('.autoList.housenumberAutoList').replaceWith(autoList)
          }
        } else {
          $('.autoList.housenumberAutoList').remove()
          isAutoListAddressVisible = false;

        }
      });
  } else {
    $('.autoList.housenumberAutoList').remove()
    isAutoListAddressVisible = false;
  }
})

$(document).on('click', '.addressItem', function () {
  $('#shop_address').val($(this).attr('data-attr-val'))
  isAutoListAddressVisible = false;
})

$(document).on('click', function () {
  $('.autoList').remove()
  isAutoListAddressVisible = false
  isAutoListCityVisible = false;
})