<?php

namespace App\Service\Cart;

use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartManager
{
    private $cart;
    private $session;
    private $articleRepository;

    public function __construct(SessionInterface $session, ArticleRepository $articleRepository)
    {
        $this->session = $session;
        $this->cart = $session->get('cart', []);
        $this->articleRepository = $articleRepository;
    }

    public function plusMinus(int $id, string $mode)
    {
        switch ($mode) {
            case 'add':
                (isset($this->cart[$id])) ? $this->cart[$id]++ : $this->cart[$id] = 1;
                break;
            case 'reduce':
                if ($this->cart[$id] == 1) {
                    unset($this->cart[$id]);
                } else {
                    $this->cart[$id]--;
                }
                break;
            case 'remove':
                unset($this->cart[$id]);
                break;
            
            default:
                throw new \Exception("$mode is an unknown operation", 1);
                break;
        }

        $this->session->set('cart', $this->cart);
    }

    public function getFullCart()
    {
        if (empty($this->cart)) {
            return [];
        }
        foreach ($this->cart as $id => $quantity) {
            $article = $this->articleRepository->find($id);
            $shopId = $article->getShop()->getId();
            if (!isset($fullcart[$shopId]['totalPrice'])) {
                $fullcart[$shopId]['shop'] = $article->getShop();
                $fullcart[$shopId]['totalPrice'] = 0;
            }

            $fullcart[$shopId]['articles'][$id] = [
                'article' => $article,
                'quantity' => $quantity
            ];
            $fullcart[$shopId]['totalPrice'] += ($fullcart[$shopId]['articles'][$id]['article']->getPrice() * $quantity);
        }
        return $fullcart;
    }

    public function getCartCount()
    {
        return array_sum($this->cart);
    }

    /**
     * Get the value of cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set the value of cart
     *
     * @return  self
     */
    public function setCart($cart)
    {
        $this->session->set('cart', $cart);
        $this->cart = $cart;

        return $this;
    }
}
