<?php

namespace App\Service\Utilities;

use App\Entity\File;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class CascadeCleaner
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function deleteFile(File $file)
    {
        $user = $file->getUser();
        $user->removeFile($file);
        $this->entityManager->persist($user);

        $shop = $file->getShop();
        if ($shop) {
            $shop->removeFile($file);
            $article = $file->getArticle();
            if ($article) {
                $setter = 'set' . $file->getField();
                $article->$setter(null);
                $article->removeFile($file);
                $this->entityManager->persist($article);
            } else {
                $shop->setMainImage(null);
            }
            $this->entityManager->persist($shop);
        }

        $article = $file->getArticle();
        if ($article) {
            $article->removeFile($file);
            $this->entityManager->persist($article);
        }

        try {
            unlink($file->getAbsolutePath());
        } catch (\Throwable $th) {
            //TODO
        }
        $this->entityManager->remove($file);
        $this->entityManager->flush();

        return true;
    }

    public function deleteArticle(Article $article)
    {
        $shop = $article->getShop();
        $shop->removeArticle($article);
        $this->entityManager->persist($shop);

        foreach ($article->getFiles() as $file) {
            $this->deleteFile($file);
        }

        $this->entityManager->remove($article);
        $this->entityManager->flush();

        return true;
    }

    public function deleteShop(Shop $shop)
    {
        foreach ($shop->getArticles() as $article) {
            $this->deleteArticle($article);
        }

        if ($file = $shop->getMainImage()) {
            $this->deleteFile($file);
        }
        if ($orders = $shop->getOrders()) {
            foreach ($orders as $order) {
                $shop->removeOrder($order);
            }
            $this->entityManager->persist($shop);
        }

        $user = $shop->getUser();
        $user->setShop(null);
        $this->entityManager->persist($user);

        $this->entityManager->remove($shop);
        $this->entityManager->flush();

        return true;
    }

    public function deleteUser(User $user)
    {
        if ($shop = $user->getShop()) {
            $this->deleteShop($shop);
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return true;
    }
}
