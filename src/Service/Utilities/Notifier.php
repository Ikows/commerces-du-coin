<?php

namespace App\Service\Utilities;

use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Notifier
{
    private $mailer;
    private $router;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $router, $adminMail, $noReplyMail)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->adminMail = $adminMail;
        $this->noReplyMail = $noReplyMail;
    }

    public function notify(string $job, ?string $route, $entity = null)
    {
        switch ($job) {
            case 'new_shop':
                $params = [
                    'shopSlug' => $entity->getShopSlug()
                ];
                $uri = $this->router->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL);
                $template = $this->getTemplate($this->adminMail, 'Un nouvelle boutique a été crée', $job);

                $context = $template->getContext();
                $context['uri'] = $uri;
                $template->context($context);
                break;
            case 'shop_toggle':
                $params = [
                    'shopSlug' => $entity->getShopSlug()
                ];
                $uri = $this->router->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL);
                $template = $this->getTemplate($entity->getEmail(), 'Votre boutique a été activée !', $job);

                $context = $template->getContext();
                $context['uri'] = $uri;
                $template->context($context);
                break;
            case 'order_merchant':
                $params = [
                    'id' => $entity->getId()
                ];
                $uri = $this->router->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL);
                $template = $this->getTemplate($entity->getShop()->getEmail(), 'Une nouvelle commande pour votre boutique', $job);

                $context = $template->getContext();
                $context['uri'] = $uri;
                $context['order'] = $entity;
                $template->context($context);
                break;
            case 'order_customer':
                $template = $this->getTemplate($entity->getCustomerMail(), 'Récapitulatif de votre commande', $job);

                $context = $template->getContext();
                $context['order'] = $entity;
                $template->context($context);
                break;
            default:
                # code...
                break;
        }

        $this->mailer->send($template);
    }

    private function getTemplate(string $to, string $subject, string $tpl)
    {
        return (new TemplatedEmail())
                    ->from(new Address($this->noReplyMail, 'Mon shop local'))
                    ->to($to)
                    ->subject($subject)
                    ->htmlTemplate("notifications/$tpl.html.twig");
    }
}
