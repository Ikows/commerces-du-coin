<?php

namespace App\Service\File;

use App\Entity\File;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileHandler
{
    private $slugger;
    private $entityManager;

    public function __construct(SluggerInterface $slugger, EntityManagerInterface $manager)
    {
        $this->slugger = $slugger;
        $this->entityManager = $manager;
    }

    public function handle(UploadedFile $file, $shopImages)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        // Move the file to the directory where brochures are stored
        try {
            $file->move(
                $shopImages,
                $newFilename
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }
        return $newFilename;
    }

    public function createFile(
        string $location,
        string $filename,
        string $mime,
        string $field,
        User $user,
        Shop $shop = null,
        Article $article = null
    ): File {
        $file = new File();
        $file
            ->setLocation($location)
            ->setFilename($filename)
            ->setMimeType($mime)
            ->setField($field)
            ->setUser($user);
        if ($shop) {
            $file->setShop($shop);
        }
        if ($article) {
            $file->setArticle($article);
        }
            
        $this->entityManager->persist($file);
        $this->entityManager->flush();
        
        return $file;
    }
}
