<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\FileType;
use App\Repository\FileRepository;
use App\Service\Utilities\CascadeCleaner;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/file")
 */
class FileController extends AbstractController
{
    /**
     * @Route("/{id}/delete", name="file_delete", methods={"GET"})
     */
    public function delete(Request $request, File $file, CascadeCleaner $cascadeCleaner): Response
    {
        $user = $this->getUser();

        // Wrong User.
        if ($user !== $file->getUser() && !$this->isGranted('ROLE_ADMIN')) {
                $this->addFlash('error', 'Action interdite');
            if ($dest = $request->query->get('dest')) {
                return $this->redirect($dest);
            }
            return $this->redirectToRoute('home', []);
        }

        // Deletion.
        if ($cascadeCleaner->deleteFile($file)) {
            $this->addFlash('success', 'Le fichier a été supprimé');
        } else {
            throw new \Exception("Error Processing file", 1);
        }

        // Redirection.
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('OK');
        } else {
            if ($dest = $request->query->get('dest')) {
                return $this->redirect($dest);
            }
            return $this->redirectToRoute('home');
        }
    }
}
