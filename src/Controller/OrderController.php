<?php

namespace App\Controller;

use App\Entity\Shop;
use App\Entity\Order;
use App\Form\OrderType;
use App\Service\Cart\CartManager;
use App\Repository\OrderRepository;
use App\Service\Utilities\Notifier;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/{shopSlug}/index", name="order_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function index(Shop $shop, OrderRepository $orderRepository): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        return $this->render('order/index.html.twig', [
            'orders' => $orderRepository->findByShop($shop),
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/create/{shopSlug}", name="order_create", methods={"GET","POST"})
     */
    public function new(Shop $shop, Request $request, CartManager $cartManager, Notifier $notifier): Response
    {
        $cart = $cartManager->getFullCart();
        $sessionCart = $cartManager->getCart();
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $content = [];
            $content['shop'] = $shop->getAllInfos();
            foreach ($cart[$shop->getId()]['articles'] as $article) {
                $articleInfos = $article['article']->getAllInfos();
                $articleInfos['quantity'] = $article['quantity'];
                $content['articles'][] = $articleInfos;
                unset($sessionCart[$article['article']->getId()]);
            }

            $order
                ->setCreatedAt(new \DateTime())
                ->setNumber($shop->getId() . time())
                ->setTotal($cart[$shop->getId()]['totalPrice'])
                ->setShop($shop)
                ->setContent($content);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();

            
            $notifier->notify('order_merchant', 'order_show', $order);
            $notifier->notify('order_customer', null, $order);

            $cartManager->setCart($sessionCart);

            return $this->redirectToRoute('shop_order_done', ['shopSlug' => $shop->getShopSlug()]);
        }

        return $this->render('order/new.html.twig', [
            'shopCart' => $cart[$shop->getId()],
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function show(Order $order): Response
    {
        if ($this->getUser() !== $order->getShop()->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        return $this->render('order/show.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/{id}", name="order_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->getUser() !== $order->getShop()->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($order);
            $entityManager->flush();
        }

        return $this->redirectToRoute('order_index');
    }
}
