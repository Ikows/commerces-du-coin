<?php

namespace App\Controller;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GeoApiController extends AbstractController
{
    /**
     * @Route("/geo/api/auto", name="geo_api_autocomplete")
     */
    public function index(Request $request): Response
    {
        $qParam = $request->query->get('q');
        $typeParam = $request->query->get('type');
        $cityCodeParam = $request->query->get('citycode') ?? '';
        $client = HttpClient::createForBaseUri('https://api-adresse.data.gouv.fr/search');
        $response = $client->request('GET', '', [
            'query' => [
                'q' => $qParam,
                'type' => $typeParam,
                'citycode' => $cityCodeParam,
                'autocomplete' => 1,
            ]
        ]);
        return new JsonResponse($response->toArray());
    }
}
