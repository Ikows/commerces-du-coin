<?php

namespace App\Controller;

use App\Repository\ShopRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ShopRepository $shopRepo, SessionInterface $session): Response
    {
        $shops = $shopRepo->findBy(['isValid' => true]);
        shuffle($shops);
        return $this->render('home/index.html.twig', [
            'shops' => $shops,
        ]);
    }
}
