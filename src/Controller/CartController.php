<?php

namespace App\Controller;

use App\Entity\Article;
use App\Service\Cart\CartManager;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/cart")
 */
class CartController extends AbstractController
{
    /**
     * @Route("/", name="cart")
     */
    public function index(CartManager $cartManager): Response
    {
        $cart = $cartManager->getFullCart();

        return $this->render('cart/index.html.twig', [
            'cart' => $cart,
            'is_cart' => true,
        ]);
    }

    /**
     * @Route("/change/{id}/{mode}", name="cart_change")
     */
    public function change(int $id, string $mode, Request $request, CartManager $cartManager): Response
    {
        $cartManager->plusMinus($id, $mode);

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('OK');
        } else {
            if ($dest = $request->query->get('dest')) {
                $this->addFlash('success', ($mode == 'add') ? 'L\'article a été ajouté au panier' : 'L\'article a été retiré du panier');
                return $this->redirect($dest);
            }
            return $this->redirectToRoute('cart');
        }
    }
}
