<?php

namespace App\Controller;

use App\Entity\Shop;
use App\Form\ShopType;
use App\Service\File\FileHandler;
use App\Repository\ShopRepository;
use App\Service\Utilities\Notifier;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Utilities\CascadeCleaner;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ShopController extends AbstractController
{
    /**
     * @Route("/shops", name="shop_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(ShopRepository $shopRepository): Response
    {
        return $this->render('shop/index.html.twig', [
            'shops' => $shopRepository->findAll(),
        ]);
    }

    /**
     * @Route("/shop/new", name="shop_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function new(Request $request,
        SluggerInterface $slugger,
        FileHandler $fileHandler,
        ShopRepository $shopRepository,
        Notifier $notifier
    ): Response {
        // Si l'user a déja un Shop.
        $user = $this->getUser();
        if ($user->getShop()) {
            $this->addFlash('error', 'Vous avez déjà une boutique');
            return $this->redirectToRoute('home');
        }

        $shop = new Shop();
        $form = $this->createForm(ShopType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateTime = new \DateTime();
            $shop->setShopSlug($slugger->slug($shop->getBrand()));
            if ($shopRepository->findBy(['shopSlug' => $shop->getShopSlug()])) {
                $shop->setShopSlug($shop->getShopSlug() . '-bis');
            }
            $shop
                ->setCreatedAt($dateTime)
                ->setUpdatedAt($dateTime)
                ->setIsValid(false);

            $entityManager = $this->getDoctrine()->getManager();

            $file = $form->get('mainImage')->getData();
            if ($file) {
                $newFilename = $fileHandler->handle($file, $this->getParameter('shop_images'));
                $fullPath = $this->getParameter('shop_images') . '/' . $newFilename;
                $ext = pathinfo($fullPath, PATHINFO_EXTENSION);
                $func = "imagecreatefrom$ext";
                $im = $func($fullPath);
                $im2 = imagecrop($im, [
                    'x' => $form->get('imageX')->getData(),
                    'y' => $form->get('imageY')->getData(),
                    'width' => $form->get('imageW')->getData(),
                    'height' => $form->get('imageH')->getData()
                    ]);
                if ($im2 !== false) {
                    imagepng($im2, $fullPath);
                    imagedestroy($im2);
                }
                imagedestroy($im);

                $entityManager->persist($shop);
                $entityManager->flush();

                $mime = mime_content_type($fullPath);
                $entityFile = $fileHandler->createFile($this->getParameter('shop_images'), $newFilename, $mime, 'mainImage', $user, $shop);
                $shop->setMainImage($entityFile);
            }

            $shop->setUser($user);
            $entityManager->persist($shop);
            $entityManager->flush();

            $notifier->notify('new_shop', 'shop_edit', $shop);

            return $this->redirectToRoute('article_index', ['shopSlug' => $shop->getShopSlug()]);
        }

        return $this->render('shop/new.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{shopSlug}", name="shop_show", methods={"GET"})
     */
    public function show(Shop $shop, ArticleRepository $articlesRepo): Response
    {
        if (!$shop->getIsValid() && $shop->getUser() !== $this->getUser()) {
            return $this->redirectToRoute('home');
        }
        $articles = $articlesRepo->findBy(['shop' => $shop, 'isAvailable' => true]);
        return $this->render('shop/show.html.twig', [
            'shop' => $shop,
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/{shopSlug}/shop/edit", name="shop_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function edit(Shop $shop, Request $request, FileHandler $fileHandler): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        $form = $this->createForm(ShopType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shop->setUpdatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();

            $file = $form->get('mainImage')->getData();
            if ($file) {
                $newFilename = $fileHandler->handle($file, $this->getParameter('shop_images'));
                $fullPath = $this->getParameter('shop_images') . '/' . $newFilename;
                $ext = pathinfo($fullPath, PATHINFO_EXTENSION);
                $func = "imagecreatefrom$ext";
                $im = $func($fullPath);
                $im2 = imagecrop($im, [
                    'x' => $form->get('imageX')->getData(),
                    'y' => $form->get('imageY')->getData(),
                    'width' => $form->get('imageW')->getData(),
                    'height' => $form->get('imageH')->getData()
                    ]);
                if ($im2 !== false) {
                    imagepng($im2, $fullPath);
                    imagedestroy($im2);
                }
                imagedestroy($im);
                
                $entityManager->persist($shop);
                $entityManager->flush();

                $mime = mime_content_type($fullPath);
                $user = $this->getUser();
                $entityFile = $fileHandler->createFile($this->getParameter('shop_images'), $newFilename, $mime, 'mainImage', $user, $shop);
                $shop->setMainImage($entityFile);
            }

            $entityManager->persist($shop);
            $entityManager->flush();

            return $this->redirectToRoute('article_index', ['shopSlug' => $shop->getShopSlug()]);
        }

        return $this->render('shop/edit.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{shopSlug}", name="shop_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function delete(Request $request, Shop $shop, CascadeCleaner $cascadeCleaner): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        if ($this->isCsrfTokenValid('delete' . $shop->getId(), $request->request->get('_token'))) {
            // Deletion.
            if ($cascadeCleaner->deleteShop($shop)) {
                $this->addFlash('success', 'La boutique a été supprimé');
            } else {
                throw new \Exception("Error Processing Shop", 1);
            }

            // Redirection.
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse('OK');
            } else {
                if ($dest = $request->query->get('dest')) {
                    return $this->redirect($dest);
                }
                return $this->redirectToRoute('home');
            }
        }
    }

    /**
     * @Route("/shop/{id}/toggle/{val}", name="shop_toggle_active", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function toggleActive(Shop $shop, int $val, EntityManagerInterface $entityManager, Notifier $notifier)
    {
        $bool = $val == 1 ? true : false;
        $shop->setIsValid($bool);
        $entityManager->persist($shop);
        $entityManager->flush();

        if ($bool) {
            $notifier->notify('shop_toggle', 'shop_show', $shop);
        }

        $this->addFlash('success', 'La boutique a été ' . ($bool ? 'activée' : 'désactivée'));
        return $this->redirectToRoute('shop_edit', ['shopSlug' => $shop->getShopSlug()]);
    }

    /**
     * @Route("/shop/{shopSlug}/order/done", name="shop_order_done", methods={"GET"})
     */
    public function orderDone(Shop $shop)
    {
        return $this->render('shop/order_done.html.twig', [
            'shop' => $shop,
        ]);
    }
}
