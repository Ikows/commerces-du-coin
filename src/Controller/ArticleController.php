<?php

namespace App\Controller;

use App\Entity\Shop;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Service\File\FileHandler;
use App\Repository\ArticleRepository;
use App\Service\Utilities\CascadeCleaner;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController
{
    /**
     * @Route("/{shopSlug}/articles", name="article_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function index(Shop $shop, ArticleRepository $articleRepository): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findByShop($shop),
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/{shopSlug}/article/new", name="article_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function new(Shop $shop, Request $request, SluggerInterface $slugger, FileHandler $fileHandler, ArticleRepository $articleRepository): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        $user = $this->getUser();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $article->setArticleSlug($slugger->slug($article->getName()));
            if ($articleRepository->findBy(['articleSlug' => $article->getArticleSlug(), 'shop' => $shop])) {
                $article->setArticleSlug($article->getArticleSlug() . '-bis');
            }
            $dateTime = new \DateTime();
            $article
                ->setCreatedAt($dateTime)
                ->setUpdatedAt($dateTime)
                ->setShop($shop);

            $entityManager = $this->getDoctrine()->getManager();

            $file = $form->get('mainImage')->getData();
            if ($file) {
                $newFilename = $fileHandler->handle($file, $this->getParameter('article_images'));
                $fullPath = $this->getParameter('article_images') . '/' . $newFilename;
                $ext = pathinfo($fullPath, PATHINFO_EXTENSION);
                $func = "imagecreatefrom$ext";
                $im = $func($fullPath);
                $im2 = imagecrop($im, [
                    'x' => $form->get('mainImageX')->getData(),
                    'y' => $form->get('mainImageY')->getData(),
                    'width' => $form->get('mainImageW')->getData(),
                    'height' => $form->get('mainImageH')->getData()
                    ]);
                if ($im2 !== false) {
                    imagepng($im2, $fullPath);
                    imagedestroy($im2);
                }
                imagedestroy($im);
                                
                $entityManager->persist($article);
                $entityManager->flush();

                $mime = mime_content_type($fullPath);
                $entityFile = $fileHandler->createFile($this->getParameter('article_images'), $newFilename, $mime, "mainImage", $user, $shop, $article);
                $article->setMainImage($entityFile);
            }
            
            // Au cas où il n'ya pas d'image principale.
            $entityManager->persist($article);
            $entityManager->flush();

            //Sec imgs
            for ($i = 1; $i < 5; $i++) {
                $file = $form->get("secImage$i")->getData();
                if ($file) {
                    $newFilename = $fileHandler->handle($file, $this->getParameter('article_images'));
                    $fullPath = $this->getParameter('article_images') . '/' . $newFilename;
                    $ext = pathinfo($fullPath, PATHINFO_EXTENSION);
                    $func = "imagecreatefrom$ext";
                    $im = $func($fullPath);
                    $im2 = imagecrop($im, [
                        'x' => $form->get("XsecImage$i")->getData(),
                        'y' => $form->get("YsecImage$i")->getData(),
                        'width' => $form->get("WsecImage$i")->getData(),
                        'height' => $form->get("HsecImage$i")->getData()
                        ]);
                    if ($im2 !== false) {
                        imagepng($im2, $fullPath);
                        imagedestroy($im2);
                    }
                    imagedestroy($im);

                    $mime = mime_content_type($fullPath);
                    $entityFile = $fileHandler->createFile($this->getParameter('article_images'), $newFilename, $mime, "secImage$i", $user, $shop, $article);

                    $article->setSecImg($entityFile, $i);
                }
            }
            //Sec imgs

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('article_index', ['shopSlug' => $shop->getShopSlug()]);
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/{shopSlug}/article/{articleSlug}", name="article_show", methods={"GET"})
     */
    public function show(Shop $shop, Article $article): Response
    {
        if (!$this->isGranted('ROLE_ADMIN') && $this->getUser() !== $shop->getUser() && !$article->getIsAvailable()) {
            return $this->redirectToRoute('home');
        }
        return $this->render('article/show.html.twig', [
            'shop' => $shop,
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{shopSlug}/article/{articleSlug}/edit", name="article_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function edit(Shop $shop, Article $article, Request $request, SluggerInterface $slugger, FileHandler $fileHandler): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateTime = new \DateTime();
            $article->setUpdatedAt($dateTime);

            $file = $form->get('mainImage')->getData();
            if ($file) {
                $newFilename = $fileHandler->handle($file, $this->getParameter('article_images'));
                $fullPath = $this->getParameter('article_images') . '/' . $newFilename;
                $ext = pathinfo($fullPath, PATHINFO_EXTENSION);
                $func = "imagecreatefrom$ext";
                $im = $func($fullPath);
                $im2 = imagecrop($im, [
                    'x' => $form->get('mainImageX')->getData(),
                    'y' => $form->get('mainImageY')->getData(),
                    'width' => $form->get('mainImageW')->getData(),
                    'height' => $form->get('mainImageH')->getData()
                    ]);
                if ($im2 !== false) {
                    imagepng($im2, $fullPath);
                    imagedestroy($im2);
                }
                imagedestroy($im);
                                
                $entityManager->persist($article);
                $entityManager->flush();

                $mime = mime_content_type($fullPath);
                $entityFile = $fileHandler->createFile($this->getParameter('article_images'), $newFilename, $mime, "mainImage", $user, $shop, $article);
                $article->setMainImage($entityFile);
            }

            //Sec imgs
            for ($i = 1; $i < 5; $i++) {
                $file = $form->get("secImage$i")->getData();
                if ($file) {
                    $newFilename = $fileHandler->handle($file, $this->getParameter('article_images'));
                    $fullPath = $this->getParameter('article_images') . '/' . $newFilename;
                    $ext = pathinfo($fullPath, PATHINFO_EXTENSION);
                    $func = "imagecreatefrom$ext";
                    $im = $func($fullPath);
                    $im2 = imagecrop($im, [
                        'x' => $form->get("XsecImage$i")->getData(),
                        'y' => $form->get("YsecImage$i")->getData(),
                        'width' => $form->get("WsecImage$i")->getData(),
                        'height' => $form->get("HsecImage$i")->getData()
                        ]);
                    if ($im2 !== false) {
                        imagepng($im2, $fullPath);
                        imagedestroy($im2);
                    }
                    imagedestroy($im);

                    $mime = mime_content_type($fullPath);
                    $entityFile = $fileHandler->createFile($this->getParameter('article_images'), $newFilename, $mime, "secImage$i", $user, $shop, $article);

                    $article->setSecImg($entityFile, $i);
                }
            }
            //Sec imgs


            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('article_index', ['shopSlug' => $shop->getShopSlug()]);
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/{shopSlug}/article/{articleSlug}", name="article_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MERCHANT')")
     */
    public function delete(Shop $shop, Request $request, Article $article, CascadeCleaner $cascadeCleaner): Response
    {
        if ($this->getUser() !== $shop->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'Action interdite');
        return $this->redirectToRoute('home', []);
        }

        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token'))) {
            // Deletion.
            if ($cascadeCleaner->deleteArticle($article)) {
                $this->addFlash('success', 'L\'article a été supprimé');
            } else {
                throw new \Exception("Error Processing Article", 1);
            }

            // Redirection.
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse('OK');
            } else {
                if ($dest = $request->query->get('dest')) {
                    return $this->redirect($dest);
                }
                return $this->redirectToRoute('article_index', ['shopSlug' => $shop->getShopSlug()]);
            }
        }
    }
}
