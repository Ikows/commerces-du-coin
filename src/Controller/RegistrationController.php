<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\EmailVerifier;
use App\Security\UserAuthenticator;
use Symfony\Component\Mime\Address;
use App\Form\CustomerRegistrationFormType;
use App\Form\MerchantRegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("/register/{type}", name="app_register")
     */
    public function register(
        string $type,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response {

        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $user = new User();

        switch ($type) {
            case 'merchant':
                $user->setRoles(['ROLE_MERCHANT']);
                $form = $this->createForm(MerchantRegistrationFormType::class, $user);
                break;
            case 'customer':
                $user->setRoles(['ROLE_CUSTOMER']);
                $form = $this->createForm(CustomerRegistrationFormType::class, $user);
                break;
            default:
                break;
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            // Generate token.
            $verifToken = hash('sha256', str_shuffle('le chocolat on aime pas ça') . time());
            $user->setVerifToken($verifToken);
            $user->setTokenValidity(time() + (3600 * 48));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from(new Address($this->getParameter('admin_mail'), 'Mon shop local'))
                    ->to($user->getEmail())
                    ->subject('Veuillez confirmer votre e-mail')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );

            $this->addFlash('success', 'Un email de vérification a été envoyé à ' . $user->getEmail());

            return $this->redirectToRoute('home');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'registrationType' => $type,
        ]);
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(
        Request $request,
        UserRepository $userRepository,
        GuardAuthenticatorHandler $guardHandler,
        UserAuthenticator $authenticator,
        EntityManagerInterface $entityManager
    ): Response {

        $token = $request->query->get('token');

        if ($user = $userRepository->findOneByVerifToken($token)) {
            if ($user->getTokenValidity() > time()) {
                $user->setIsVerified(true);
                $entityManager->persist($user);
                $entityManager->flush();
                $this->addFlash('success', 'Votre adresse e-mail a été vérifiée avec succès. Bienvenue !');
                return $guardHandler->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $authenticator,
                    'main',
                );
            } else {
                $this->addFlash('error', 'Le lien d\'activation est périmé.');
            }
        } else {
            $this->addFlash('error', 'Le lien d\'activation est invalide');
        }
        return $this->redirectToRoute('home');
    }
}
