<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Form\ChangePasswordFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/index", name="user_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        if (!$this->isGranted('ROLE_ADMIN') && $this->getUser() !== $user) {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Les modifications ont été enregistrées');
            return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        // Wrong User.
        if (!$this->isGranted('ROLE_ADMIN') && $this->getUser() !== $user) {
                $this->addFlash('error', 'Action interdite');
            if ($dest = $request->query->get('dest')) {
                return $this->redirect($dest);
            }
            return $this->redirectToRoute('home', []);
        }

        // Deletion.
        $user->setToDelete(true);
        $entityManager->persist($user);

        if ($shop = $user->getShop()) {
            $shop->setIsValid(false);
            $entityManager->persist($shop);
        }

        $entityManager->flush();
        $this->addFlash('success', 'Votre demande de suppression de compte a été enregistrée et sera bientôt executée');
        $this->addFlash('success', 'Votre boutique a été desactivée');

        // Redirection.
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('OK');
        } else {
            if ($dest = $request->query->get('dest')) {
                return $this->redirect($dest);
            }
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("/{id}/change-password", name="user_change_password", methods={"GET", "POST"})
     */
    public function changePassword(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if (!$this->isGranted('ROLE_ADMIN') && $this->getUser() !== $user) {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encodedPassword = $passwordEncoder->encodePassword(
                $user,
                $form->get('plainPassword')->getData()
            );
            $user->setPassword($encodedPassword);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Le nouveau mot de passe a été enregistré');

            return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
        }

        return $this->render('reset_password/reset.html.twig', [
            'resetForm' => $form->createView(),
        ]);
    }
}
