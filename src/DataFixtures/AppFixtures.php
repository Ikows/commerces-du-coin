<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Article;
use App\Entity\File;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder, SluggerInterface $slugger)
    {
        $this->encoder = $encoder;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $user = new User();
            $password = $this->encoder->encodePassword($user, '123456');
            $user
                ->setEmail('iko@iko.iko')
                ->setName($faker->lastname)
                ->setFirstname($faker->firstname)
                ->setAddress($faker->streetAddress)
                ->setZipCode($faker->postcode)
                ->setCity($faker->city)
                ->setPhone($faker->phoneNumber)
                ->setPassword($password)
                ->setIsVerified(true)
                ->setRoles(['ROLE_ADMIN']);
            $manager->persist($user);

        for ($i = 0; $i < 25; $i++) {
            $user = new User();
            $password = $this->encoder->encodePassword($user, '123456');
            $user
                ->setEmail($faker->email)
                ->setName($faker->lastname)
                ->setFirstname($faker->firstname)
                ->setAddress($faker->streetAddress)
                ->setZipCode($faker->postcode)
                ->setCity($faker->city)
                ->setIsVerified(true)
                ->setPhone($faker->phoneNumber)
                ->setPassword($password)
                ->setRoles(['ROLE_MERCHANT']);
            $manager->persist($user);

            $shop = new Shop();
            $shop
                ->setCreatedAt(new DateTime())
                ->setUpdatedAt(new DateTime())
                ->setIsValid(true)
                ->setBrand($faker->sentence(3))
                ->setAddress($faker->streetAddress)
                ->setZipCode($faker->postcode)
                ->setCity($faker->city)
                ->setPhone($faker->phoneNumber)
                ->setEmail($faker->email)
                ->setPresentation($faker->text(500))
                ->setOrderMessage($faker->text(500))
                ->setThankMessage($faker->text(500))
                ->setShopSlug($this->slugger->slug($shop->getBrand()))
                ->setUser($user);
                $manager->persist($shop);

                $file = new File();
                $file
                    ->setLocation("C:\xampp\htdocs\commerces-du-coin\public/uploads/shop-images")
                    ->setFilename('shop.jpeg')
                    ->setMimeType('image/jpeg')
                    ->setField('mainImage')
                    ->setUser($user)
                    ->setShop($shop);
                    $manager->persist($file);

                $shop->setMainImage($file);
                $manager->persist($shop);

            for ($y = 0; $y < 10; $y++) {
                $article = new Article();
                $article
                    ->setCreatedAt(new DateTime())
                    ->setUpdatedAt(new DateTime())
                    ->setName($faker->sentence(3))
                    ->setTeaser($faker->text(200))
                    ->setDescription($faker->text(500))
                    ->setSeeMore($faker->paragraphs(random_int(0, 8), true))
                    ->setIsAvailable(true)
                    ->setPrice($faker->randomFloat(2, 1, 500));

                $manager->persist($article);

                $imgs = [];
                for ($x = 0; $x < 5; $x++) {
                    $imgs[$x] = new File();
                    $imgs[$x]
                        ->setLocation('C:\xampp\htdocs\commerces-du-coin\public/uploads/article-images')
                        ->setFilename('article-placeholder.png')
                        ->setMimeType('image/jpeg');
                    if ($x == 0) {
                        $imgs[$x]->setField('mainImage');
                    } else {
                        $imgs[$x]->setField("secImage$x");
                    }
                    $imgs[$x]
                        ->setUser($user)
                        ->setShop($shop)
                        ->setArticle($article);
                    $manager->persist($imgs[$x]);
                }

                $article
                    ->setMainImage($imgs[0])
                    ->setSecImage1($imgs[1])
                    ->setSecImage2($imgs[2])
                    ->setSecImage3($imgs[3])
                    ->setSecImage4($imgs[4])
                    ->setArticleSlug($this->slugger->slug($article->getName()))
                    ->setShop($shop);
                $manager->persist($article);
            }
        }
        $manager->flush();
    }
}
