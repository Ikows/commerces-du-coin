<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerFirstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customerMail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $CustomerAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerZipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerCity;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $content = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $customerMessage;

    /**
     * @ORM\ManyToOne(targetEntity=Shop::class, inversedBy="orders")
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(?float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCustomerName(): ?string
    {
        return $this->customerName;
    }

    public function setCustomerName(string $customerName): self
    {
        $this->customerName = $customerName;

        return $this;
    }

    public function getCustomerFirstname(): ?string
    {
        return $this->customerFirstname;
    }

    public function setCustomerFirstname(?string $customerFirstname): self
    {
        $this->customerFirstname = $customerFirstname;

        return $this;
    }

    public function getCustomerMail(): ?string
    {
        return $this->customerMail;
    }

    public function setCustomerMail(string $customerMail): self
    {
        $this->customerMail = $customerMail;

        return $this;
    }

    public function getCustomerPhone(): ?string
    {
        return $this->customerPhone;
    }

    public function setCustomerPhone(?string $customerPhone): self
    {
        $this->customerPhone = $customerPhone;

        return $this;
    }

    public function getCustomerAddress(): ?string
    {
        return $this->CustomerAddress;
    }

    public function setCustomerAddress(?string $CustomerAddress): self
    {
        $this->CustomerAddress = $CustomerAddress;

        return $this;
    }

    public function getCustomerZipCode(): ?string
    {
        return $this->customerZipCode;
    }

    public function setCustomerZipCode(?string $customerZipCode): self
    {
        $this->customerZipCode = $customerZipCode;

        return $this;
    }

    public function getCustomerCity(): ?string
    {
        return $this->customerCity;
    }

    public function setCustomerCity(?string $customerCity): self
    {
        $this->customerCity = $customerCity;

        return $this;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function setContent(?array $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCustomerMessage(): ?string
    {
        return $this->customerMessage;
    }

    public function setCustomerMessage(?string $customerMessage): self
    {
        $this->customerMessage = $customerMessage;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }
}
