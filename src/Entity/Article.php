<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $teaser;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAvailable;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Shop::class, inversedBy="articles")
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $articleSlug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $seeMore;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="article")
     */
    private $files;

    /**
     * @ORM\OneToOne(targetEntity=File::class, cascade={"persist", "remove"})
     */
    private $secImage1;

    /**
     * @ORM\OneToOne(targetEntity=File::class, cascade={"persist", "remove"})
     */
    private $secImage2;

    /**
     * @ORM\OneToOne(targetEntity=File::class, cascade={"persist", "remove"})
     */
    private $secImage3;

    /**
     * @ORM\OneToOne(targetEntity=File::class, cascade={"persist", "remove"})
     */
    private $secImage4;

    /**
     * @ORM\OneToOne(targetEntity=File::class, cascade={"persist", "remove"})
     */
    private $mainImage;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTeaser(): ?string
    {
        return $this->teaser;
    }

    public function setTeaser(string $teaser): self
    {
        $this->teaser = $teaser;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->isAvailable;
    }

    public function setIsAvailable(bool $isAvailable): self
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getArticleSlug(): ?string
    {
        return $this->articleSlug;
    }

    public function setArticleSlug(string $articleSlug): self
    {
        $this->articleSlug = $articleSlug;

        return $this;
    }

    public function getSeeMore(): ?string
    {
        return $this->seeMore;
    }

    public function setSeeMore(?string $seeMore): self
    {
        $this->seeMore = $seeMore;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSecImg(int $nb): ?File
    {
        $img = 'secImage' . $nb;
        return $this->$img;
    }

    public function setSecImg(?File $secImg, int $nb): self
    {
        $img = 'secImage' . $nb;
        $this->$img = $secImg;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setArticle($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getArticle() === $this) {
                $file->setArticle(null);
            }
        }

        return $this;
    }

    public function getSecImage1(): ?File
    {
        return $this->secImage1;
    }

    public function setSecImage1(?File $secImage1): self
    {
        $this->secImage1 = $secImage1;

        return $this;
    }

    public function getSecImage2(): ?File
    {
        return $this->secImage2;
    }

    public function setSecImage2(?File $secImage2): self
    {
        $this->secImage2 = $secImage2;

        return $this;
    }

    public function getSecImage3(): ?File
    {
        return $this->secImage3;
    }

    public function setSecImage3(?File $secImage3): self
    {
        $this->secImage3 = $secImage3;

        return $this;
    }

    public function getSecImage4(): ?File
    {
        return $this->secImage4;
    }

    public function setSecImage4(?File $secImage4): self
    {
        $this->secImage4 = $secImage4;

        return $this;
    }

    public function getMainImage(): ?File
    {
        return $this->mainImage;
    }

    public function setMainImage(?File $mainImage): self
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    public function getAllInfos()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
        ];
    }
}
