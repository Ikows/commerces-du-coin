<?php

namespace App\Form;

use App\Entity\Article;
use App\Form\AbstractForm;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ArticleType extends AbstractForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, $this->setup('Nom de l\'article', true))
            ->add('teaser', CKEditorType::class, [
                'label' => 'Description brève',
                'help' => 'Texte affiché dans la miniature de l\'article dans la boutique. Limité a 200 caractères',
                'config' => ['toolbar' => 'basic'],
                'constraints' => [
                    new Length([
                        'max' => 200,
                    ]),
                ],
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Description',
                'help' => 'Texte affiché a coté de l\'article sur la page de l\'article. Limité a 500 caractères',
                'config' => ['toolbar' => 'basic'],
                'constraints' => [
                    new Length([
                        'max' => 500,
                    ]),
                ],
            ])
            ->add('seeMore', CKEditorType::class, [
                'label' => 'Description Etendue',
                'help' => 'Texte affiché en dessous sur la page de l\'article. Limité a 2000 caractères',
                'config' => ['toolbar' => 'basic'],
                'constraints' => [
                    new Length([
                        'max' => 2000,
                    ]),
                ],
            ])
            ->add('price', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Prix de l\'article en euros'
                ],
                'scale' => 2,
                'label' => false,
                'required' => true,
            ])
            ->add('mainImage', FileType::class, [
                'label' => 'Image principale',
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Veuillez importer un format d\'image valide (jpg, jpeg, png).',
                    ])
                ],
                'data_class' => null,
                'required' => false,
                'help' => 'Formats acceptés : jpeg, jpg, png. Taille maximale : 1Mo. Vous pourrez recardrer l\'image pour qu\'elle corresponde au format requis (4:3)',
                'mapped' => false,
                'attr' => [
                    'data-img-number' => 0,
                ],
            ])
            ->add('mainImageX', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('mainImageY', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('mainImageW', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('mainImageH', HiddenType::class, [
                'mapped' => false,
            ])
        ;
        for ($i = 1; $i < 5; $i++) {
            $builder
                ->add("secImage$i", FileType::class, [
                    'label' => "Image secondaire n°$i",
                    'constraints' => [
                        new File([
                            'maxSize' => '1024k',
                            'mimeTypes' => [
                                'image/jpeg',
                                'image/jpg',
                                'image/png',
                            ],
                            'mimeTypesMessage' => 'Veuillez importer un format d\'image valide (jpg, jpeg, png).',
                        ])
                    ],
                    'data_class' => null,
                    'required' => false,
                    'help' => 'Formats acceptés : jpeg, jpg, png. Taille maximale : 1Mo. Vous pourrez recardrer l\'image pour qu\'elle corresponde au format requis (4:3)',
                    'mapped' => false,
                    'attr' => [
                        'data-img-number' => $i,
                    ],
                ])
                ->add("XsecImage$i", HiddenType::class, [
                    'mapped' => false,
                ])
                ->add("YsecImage$i", HiddenType::class, [
                    'mapped' => false,
                ])
                ->add("WsecImage$i", HiddenType::class, [
                    'mapped' => false,
                ])
                ->add("HsecImage$i", HiddenType::class, [
                    'mapped' => false,
                ])
            ;
        }
        $builder->add('isAvailable', CheckboxType::class, [
            'label' => 'En ligne',
            'help' => 'Cocher la case pour que l\'article apparaisse sur la boutique. Décocher pour le rendre inactif',
            'required' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
