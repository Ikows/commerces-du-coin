<?php

namespace App\Form;

use App\Entity\User;
use App\Form\AbstractForm;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserType extends AbstractForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, $this->setup('Adresse e-mail', true))
            ->add('firstname', TextType::class, $this->setup('Prénom', true))
            ->add('name', TextType::class, $this->setup('Nom', true))
            ->add('city', TextType::class, $this->setup('Ville'))
            ->add('zipCode', TextType::class, $this->setup('Code postal'))
            ->add('address', TextType::class, $this->setup('Adresse'))
            ->add('phone', TextType::class, $this->setup('Numéro de téléphone'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
