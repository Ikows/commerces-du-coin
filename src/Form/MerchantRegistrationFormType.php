<?php

namespace App\Form;

use App\Entity\User;
use App\Form\AbstractForm;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MerchantRegistrationFormType extends AbstractForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, $this->setup('Adresse e-mail', true))
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux champs mot de passe ne correspondent pas',
                'options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Veuillez entrer un mot de passe',
                        ]),
                        new Length([
                            'min' => 8,
                            'minMessage' => 'Votre mot de passe doit comprter au moins {{ limit }} caractères',
                            'max' => 4096,
                        ]),
                    ],
                ],
                'mapped' => false,
                'required' => true,
                'first_options'  => $this->setup('Mot de passe'),
                'second_options' => $this->setup('Confirmez le mot de passe'),
                ])
            ->add('firstname', TextType::class, $this->setup('Prénom', true))
            ->add('name', TextType::class, $this->setup('Nom', true))
            ->add('city', TextType::class, $this->setup('Ville'))
            ->add('zipCode', TextType::class, $this->setup('Code postal'))
            ->add('address', TextType::class, $this->setup('Adresse'))
            ->add('phone', TextType::class, $this->setup('Numéro de téléphone'))
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => 'J\'accepte les conditions générales d\'utilisation',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez approuver les conditions générales d\'utilisation',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
