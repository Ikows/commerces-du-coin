<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;

abstract class AbstractForm extends AbstractType
{
    /**
     * Undocumented function
     *
     * @param string $placeholder
     * @param boolean $label
     * @return array
     */
    public function setup(string $placeholder, bool $required = false, bool $label = false): array
    {
        return [
            'attr' => [
                'placeholder' => $placeholder,
            ],
            'required' => $required,
            'label' => $label,
        ];
    }
}
