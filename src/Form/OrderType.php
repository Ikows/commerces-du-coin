<?php

namespace App\Form;

use App\Entity\Order;
use App\Form\AbstractForm;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OrderType extends AbstractForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customerFirstname', TextType::class, $this->setup('Prénom', false))
            ->add('customerName', TextType::class, $this->setup('Nom', false))
            ->add('customerMail', EmailType::class, $this->setup('Adresse e-mail', false))
            ->add('customerCity', TextType::class, $this->setup('Ville', false))
            ->add('customerZipCode', TextType::class, $this->setup('Code postal', false))
            ->add('CustomerAddress', TextType::class, $this->setup('Adresse', false))
            ->add('customerPhone', TextType::class, $this->setup('Numéro de téléphone', false))
            ->add('customerMessage', TextareaType::class, $this->setup('Message', false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
