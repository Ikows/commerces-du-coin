<?php

namespace App\Form;

use App\Entity\Shop;
use App\Form\AbstractForm;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ShopType extends AbstractForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('brand', TextType::class, $this->setup('Nom de la boutique', true))
            ->add('city', TextType::class, [
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Ville',
                ],
                'required' => true,
                'label' => false,
                'help' => 'Pour que le géocodage soit valide, veuillez utiliser l\'autocomplétion',
            ])
            ->add('citycode', HiddenType::class, [
                'mapped' => false
            ])
            ->add('zipCode', TextType::class, $this->setup('Code Postal', true))
            ->add('address', TextType::class, [
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Adresse',
                ],
                'required' => true,
                'label' => false,
                'help' => 'Pour que le géocodage soit valide, veuillez utiliser l\'autocomplétion',
            ])
            ->add('phone', TextType::class, $this->setup('Numéro de téléphone'))
            ->add('email', EmailType::class, $this->setup('Adresse e-mail', true))
            ->add('presentation', CKEditorType::class, [
                'label' => 'Texte de présentation de la boutique',
                'help' => 'Ce texte apparait sur la vignette de votre boutique en page d\'accueil',
                'attr' => [
                ],
                'config' => ['toolbar' => 'basic'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'La longueur de la présentation doit être comprise entre 1 et 500 caractères',
                    ]),
                    new Length([
                        'min' => 1,
                        'minMessage' => 'La longueur de la présentation doit être comprise entre 1 et 500 caractères',
                        'minMessage' => 'La longueur de la présentation doit être comprise entre 1 et 500 caractères',
                        'max' => 500,
                    ]),
                ],
            ])
            ->add('orderMessage', CKEditorType::class, [
                'label' => 'Texte affiché au client avant validation de commande',
                'help' => 'Ce texte apparait juste avant que l\'utilisateur ne déclenche sa commande. Précisez par exemple les horaires d\'ouverture ou les modalités de collecte',
                'attr' => [
                ],
                'config' => ['toolbar' => 'basic'],
            ])
            ->add('thankMessage', CKEditorType::class, [
                'label' => 'Texte affiché au client après la commande',
                'help' => 'Ce texte apparait à l\'utilisateur lorsqu\'il a validé sa commande',
                'attr' => [
                ],
                'config' => ['toolbar' => 'basic'],
            ])
            ->add('mainImage', FileType::class, [
                'label' => 'Image principale',
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Veuillez importer un format d\'image valide (jpg, jpeg, png).',
                    ])
                ],
                'data_class' => null,
                'required' => false,
                'help' => 'Format acceptés : jpeg, jpg, png. Taille maximale : 2Mo. Vous pourrez recardrer l\'image pour qu\'elle corresponde au format requis (16:9)',
                'mapped' => false,
            ])
            ->add('imageX', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('imageY', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('imageW', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('imageH', HiddenType::class, [
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
        ]);
    }
}
